import {WorkerData} from "./WorkerData";

let today = new Date()
export const ProjectsData = [
    {
        id : 47321,
        name : "Projeto A",
        resp : "Júlio César Teixeira",
        startDate : "04/03/2021",
        finishDate : "",
        insumos : [
            {
                qtd : "5L",
                title : "Quiboa",
                buy : true
            },
            {
                qtd : "2L",
                title : "Água sanitária",
                buy : true
            }
        ],
        workers: [WorkerData[1]],
        alerts : [],
        documents : [
            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Relatório_financeiro.pdf",
                send : false
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Voltantes.pdf",
                send : true
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Horas_trabalhadas.pdf",
                send : false
            }
        ]
    },

    {
        id : 8832,
        name : "Projeto B",
        resp : "Carlos Alberto Barcelos do Amaral",
        startDate : "02/06/2019",
        finishDate : "",
        insumos : [
            {
                qtd : "2L",
                title : "Água sanitária",
                buy : true
            }
        ],
        workers: [WorkerData[1]],
        alerts : [],
        documents : [
            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Relatório_financeiro.pdf",
                send : false
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Voltantes.pdf",
                send : true
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Horas_trabalhadas.pdf",
                send : false
            }
        ]
    },

    {
        id : 47321,
        name : "Projeto C",
        resp : "Júlio César Teixeira",
        startDate : "04/03/2021",
        finishDate : "",
        insumos : [
            {
                qtd : "5L",
                title : "Quiboa",
                buy : false
            },
            {
                qtd : "2L",
                title : "Água sanitária",
                buy : true
            }
        ],
        workers: [WorkerData[1]],
        alerts : [],
        documents : [
            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Relatório_financeiro.pdf",
                send : false
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Voltantes.pdf",
                send : true
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Horas_trabalhadas.pdf",
                send : false
            }
        ]
    },

    {
        id : 8832,
        name : "Projeto D",
        resp : "Carlos Alberto Barcelos do Amaral",
        startDate : "02/06/2019",
        finishDate : "",
        insumos : [
            {
                qtd : "2L",
                title : "Água sanitária",
                buy : true
            }
        ],
        workers: [WorkerData[0], WorkerData[1]],
        alerts : [],
        documents : [
            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Relatório_financeiro.pdf",
                send : false
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Voltantes.pdf",
                send : true
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Horas_trabalhadas.pdf",
                send : false
            }
        ]
    },

    {
        id : 47321,
        name : "Projeto E",
        resp : "Júlio César Teixeira",
        startDate : "04/03/2021",
        finishDate : "",
        insumos : [
            {
                qtd : "5L",
                title : "Quiboa",
                buy : false
            },
            {
                qtd : "2L",
                title : "Água sanitária",
                buy : true
            }
        ],
        workers: [WorkerData[0], WorkerData[1]],
        alerts : [],
        documents : [
            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Relatório_financeiro.pdf",
                send : false
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Voltantes.pdf",
                send : true
            },

            {
                date : `${today.getDate()}/${today.getMonth()}/${today.getFullYear()} - ${today.getHours()}:${today.getMinutes()}`,
                filename : "Horas_trabalhadas.pdf",
                send : false
            }
        ]
    }

]