import './TitleStyle.scss'

export default function TitleComponent(props) {
    return (
        <div className={"titleComponent"}>
            <h1>{props.title}</h1>
        </div>
    )
}