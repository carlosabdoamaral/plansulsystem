import './SidebarStyle.scss'
import dashboard from '../../static/icons/dashboard.svg'
import folder from '../../static/icons/folder-solid.svg'
import users from '../../static/icons/users-solid.svg'
import receipt from '../../static/icons/receipt-solid.svg'
import clipboard from '../../static/icons/clipboard-list-solid.svg'

export default function SidebarComponent(props) {
    return (
        <div className={"sidebar"}>
            <ul>
                <li>
                    <img
                        src={dashboard}
                        className={props.active === "dashboard" ? "active" : ""}
                        alt={"Dashboard"}
                        onClick={() => { window.location.href = "/dashboard" }}
                    />
                </li>

                {/*<li>*/}
                {/*    <img*/}
                {/*        src={folder}*/}
                {/*        className={props.active === "documents" ? "active" : ""}*/}
                {/*        alt={"Docs"}*/}
                {/*        onClick={() => { window.location.href = "/documents" }}*/}
                {/*    />*/}
                {/*</li>*/}

                <li>
                    <img
                        src={clipboard}
                        className={props.active === "projects" ? "active" : ""}
                        alt={"Projects"}
                        onClick={() => { window.location.href = "/projects" }}
                    />
                </li>

                <li>
                    <img
                        src={users}
                        className={props.active === "users" ? "active" : ""}
                        alt={"Users"}
                        onClick={() => { window.location.href = "/employees" }}
                    />
                </li>

                <li>
                    <img
                        src={receipt}
                        className={props.active === "payment" ? "active" : ""}
                        alt={"Payment"}
                        onClick={() => { window.location.href = "/payments" }}
                    />
                </li>
            </ul>
        </div>
    )
}