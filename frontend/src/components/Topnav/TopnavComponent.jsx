import logo from '../../static/images/logo.png'
import './TopnavStyle.scss'

export default function TopnavComponent(props) {
    return (
        <nav>
            <img src={logo}/>
        </nav>
    )
}