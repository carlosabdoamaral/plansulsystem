import {Route, Routes} from 'react-router-dom'
import LoginView from "./views/login/LoginView";
import DashboardView from "./views/dashboard/DashboardView";
import DocumentsView from "./views/documents/DocumentsView";
import UsersView from "./views/users/UsersView";
import ProjectsView from "./views/projects/ProjectsView";
import ProjectDetailsView from "./views/project-details/ProjectDetailsView";

export default function ProjectRoutes() {
    return (
        <Routes>
            <Route path={"/login"} element={<LoginView/>}/>
            <Route path={"/dashboard/*"} element={<DashboardView/>}/>
            <Route path={"/documents/*"} element={<DocumentsView/>}/>
            <Route path={"/projects/"} element={<ProjectsView/>}/>
            <Route path={"/projects/details"} element={<ProjectDetailsView/>}/>
            <Route path={"/employees/*"} element={<UsersView/>}/>
        </Routes>
    )
}