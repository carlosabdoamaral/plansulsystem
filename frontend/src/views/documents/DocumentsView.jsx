import SidebarComponent from "../../components/Sidebar/SidebarComponent";
import './DocumentsStyle.scss'
import TitleComponent from "../../components/PageTitle/TitleComponent";
import {ProjectsData} from "../../data/ProjectsData";

export default function DocumentsView(props) {

    let projectsData = ProjectsData

    return (
        <main className={"documents"}>
            <SidebarComponent active={"documents"}/>
            <div className={"content"}>
                <TitleComponent title={"Documentos"}/>

                <div className={"list"}>
                    <div className={"buttons"}>
                        <button type={"button"} >Enviar documento</button>
                    </div>

                    {
                        projectsData.map( (project, i) => (
                            <SingleProject project={i}/>
                        ))
                    }
                </div>
            </div>
        </main>
    )
}

function SingleProject(props) {
    const project = ProjectsData[props.project]

    return (
        <div className={"singleProject"}>

            <div className={"nameAndId"}>
                <small>#{project.id}</small>
                <h3>{project.name}</h3>
            </div>

            <button onClick={() => { window.location.href = `documents/details` }}>Ver detalhes</button>
        </div>
    )
}