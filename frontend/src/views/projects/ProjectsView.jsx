import SidebarComponent from "../../components/Sidebar/SidebarComponent";
import TitleComponent from "../../components/PageTitle/TitleComponent";
import {ProjectsData} from "../../data/ProjectsData";
import './ProjectsStyle.scss'

export default function ProjectsView(props) {

    let projectsData = ProjectsData

    return (
        <main className={"projects"}>
            <SidebarComponent active={"projects"}/>
            <div className={"content"}>
                <TitleComponent title={"Projetos"}/>

                <div className={"list"}>
                    <div className={"buttons"}>
                        <button type={"button"} >Adicionar projeto</button>
                    </div>

                    {
                        projectsData.map( (project, i) => (
                            <SingleProject project={i}/>
                        ))
                    }
                </div>
            </div>
        </main>
    )
}

function SingleProject(props) {
    const project = ProjectsData[props.project]

    return (
        <div className={"singleProject"}>
            <div className={"nameAndId"}>
                <small>#{project.id}</small>
                <h3>{project.name}</h3>
            </div>

            <button onClick={() => { sessionStorage.setItem("projectID", props.project); window.location.href = `projects/details` }}>Ver detalhes</button>
        </div>
    )
}