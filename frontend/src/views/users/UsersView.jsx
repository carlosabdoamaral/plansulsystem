import SidebarComponent from "../../components/Sidebar/SidebarComponent";
import './UsersStyle.scss'
import TitleComponent from "../../components/PageTitle/TitleComponent";
import {WorkerData} from "../../data/WorkerData";

export default function UsersView() {
    let workerData = WorkerData

    return (
        <main className={"users-list"}>
            <SidebarComponent active={"users"}/>

            <div className={"content"}>
                <TitleComponent title={"Funcionários"}/>

                <div className={"list"}>
                    <div className={"buttons"}>
                        <button type={"button"} >Adicionar funcionário</button>
                    </div>

                    {
                        workerData.map( (worker, i) => (
                            <SingleProject workerID={i}/>
                        ))
                    }
                </div>
            </div>
        </main>
    )
}

function SingleProject(props) {
    const worker = WorkerData[props.workerID]

    return (
        <div className={"singleProject"}>

            <div className={"nameAndId"}>
                <small>#{worker.id}</small>
                <h3>{worker.name}</h3>
            </div>

            <button onClick={() => { window.location.href = `employees/details` }}>Ver detalhes</button>
        </div>
    )
}