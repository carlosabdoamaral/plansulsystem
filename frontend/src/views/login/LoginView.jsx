import './LoginStyle.scss'
import logo from '../../static/images/logo.png'
import lock from '../../static/icons/lock-solid.svg'
import mail from '../../static/icons/at-solid.svg'

export default function LoginView(props) {
    return (
        <main className={"login"}>
            <div className={"images"}>
                <img src={logo} alt={"Logo plansul"}/>

                <div className={"text"}>
                    <h3>Agora você pode gerenciar todas as suas demandas em um só lugar</h3>
                    {/*<h6>Este sistema foi planejado para você aumentar a sua produtividade na metade de tempo trabalhado</h6>*/}
                    <h6>Desenvolvido por: <br/><a href={"https://www.linkedin.com/in/carlos-alberto-barcelos-do-amaral-56a6881b3/"} target={"_blank"} rel={"noreferrer"}>Carlos Alberto Barcelos do Amaral</a></h6>
                </div>
            </div>

            <div className={"form"}>
                <h1>Olá!</h1>
                <h4>Para continuar, pedimos para que você entre na plataforma.</h4>

                <div className={"fields"}>
                    <div>
                        <input type={"email"} placeholder={"Seu e-mail"}/>
                        <img src={mail} />
                    </div>

                    <div>
                        <input type={"password"} placeholder={"Sua senha"}/>
                        <img src={lock} />
                    </div>

                    <h6>Esqueceu a senha?</h6>

                    <button>Entrar!</button>
                </div>
            </div>
        </main>
    )
}