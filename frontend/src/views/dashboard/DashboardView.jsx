import TopnavComponent from "../../components/Topnav/TopnavComponent";
import SidebarComponent from "../../components/Sidebar/SidebarComponent";
import './DashboardStyle.scss'
import TitleComponent from "../../components/PageTitle/TitleComponent";
import {ProjectsData} from "../../data/ProjectsData";
import {WorkerData} from "../../data/WorkerData";

import dashboard from '../../static/icons/dashboard.svg'
import folder from '../../static/icons/folder-solid.svg'
import users from '../../static/icons/users-solid.svg'
import receipt from '../../static/icons/receipt-solid.svg'
import clipboard from '../../static/icons/clipboard-list-solid.svg'

export default function DashboardView(props) {
    const projectsData = ProjectsData
    const workerData = WorkerData

    return (
        <main className={"dashboard"}>
            {/*<TopnavComponent/>*/}

            <div className={"main"}>
                <SidebarComponent active={"dashboard"}/>

                <div className={"content"}>
                    <TitleComponent title={"Bem vindo"}/>

                    <div className={"body"}>
                        <div className={"top-squares"}>
                            <LargeSquare title={workerData.length} subtitle={"Funcionários"} icon={users}/>
                            <LargeSquare title={projectsData.length} subtitle={"Projetos"} icon={clipboard}/>
                            <LargeSquare title={"300"} subtitle={"Documentos enviados"} icon={folder}/>
                            <LargeSquare title={"4.302"} subtitle={"Funcionários"} icon={users}/>
                            <LargeSquare title={"4.302"} subtitle={"Funcionários"} icon={users}/>
                        </div>
                    </div>
                </div>

            </div>
        </main>
    )
}

function LargeSquare(props) {
    return (
        <div className={"large-square"}>

            <div className={"top"}>
                <div>
                    <img src={props.icon}/>
                </div>
            </div>

            <div className={"middle"}>
                <p>{props.subtitle}</p>
                <h1>{props.title}</h1>
            </div>

            <div className={"bottom"}>
                <hr/>
                <small>Atualizado agora</small>
            </div>
        </div>
    )
}