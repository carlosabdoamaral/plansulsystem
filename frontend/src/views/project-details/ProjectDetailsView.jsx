import './ProjectDetailsStyle.scss'
import SidebarComponent from "../../components/Sidebar/SidebarComponent";
import TitleComponent from "../../components/PageTitle/TitleComponent";
import {ProjectsData} from "../../data/ProjectsData";
import {useEffect, useState} from "react";

export default function ProjectDetailsView(props) {
    let project = ProjectsData[parseInt(sessionStorage.getItem("projectID"))]

    return (
        <main className={"projectDetails"}>
            <SidebarComponent active={"projects"}/>

            <div className={"content"}>
                <TitleComponent title={`#${project.id} - ${project.name}`}/>
                <h4>Responsável: {project.resp} </h4>

                <div className={"all-lists"}>

                    {/*    INSUMOS    */}
                    <div className={"list"}>
                        <div className={"buttons"}>
                            <button type={"button"} >Editar</button>
                            <button type={"button"} >Enviar relatório</button>
                        </div>

                        {
                            project.insumos.map( (insumo, i) => (
                                <SingleInsumo insumo={insumo}/>
                            ))
                        }
                    </div>

                    {/*    FUNCIONÁRIOS    */}
                    <div className={"list"}>
                        <div className={"buttons"}>
                            <button type={"button"} >Editar</button>
                        </div>

                        {
                            project.workers.map( (worker, i) => (
                                <SingleWorker worker={worker}/>
                            ))
                        }
                    </div>

                    {/*    DOCUMENTOS    */}
                    <div className={"list"}>
                        <div className={"buttons"}>
                            <button type={"button"} >Editar</button>
                        </div>

                        {
                            project.documents.map( (document, i) => (
                                <SingleDocument document={document}/>
                            ))
                        }
                    </div>
                </div>
            </div>
        </main>
    )
}

function SingleInsumo(props) {
    const insumo = props.insumo

    return (
        <div className={"SingleRow"}>

            <div className={"nameAndId"}>
                <small>{insumo.qtd}</small>
                <h3>{insumo.title}</h3>
            </div>

            <h5 className={insumo.buy ? "comprado" : "naoComprado"}>{insumo.buy ? "Comprado" : "Não comprado"}</h5>
        </div>
    )
}

function SingleWorker(props) {
    const worker = props.worker

    return (
        <div className={"SingleRow"}>

            <div className={"nameAndId"}>
                <small>#{worker.id}</small>
                <h3>{worker.name}</h3>
            </div>

        </div>
    )
}

function SingleDocument(props) {
    const document = props.document

    return (
        <div className={"SingleRow"}>

            <div className={"nameAndId"}>
                <small>{document.date}</small>
                <h3>{document.filename}</h3>
            </div>

            <h5 className={document.send ? "enviado" : "naoEnviado"}>{document.send ? "Enviado" : "Não enviado"}</h5>
        </div>
    )
}