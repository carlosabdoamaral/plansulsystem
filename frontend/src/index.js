import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import ProjectRoutes from "./routes";

ReactDOM.render(
    <BrowserRouter>
      <ProjectRoutes/>
    </BrowserRouter>,
  document.getElementById('root')
);

reportWebVitals();
